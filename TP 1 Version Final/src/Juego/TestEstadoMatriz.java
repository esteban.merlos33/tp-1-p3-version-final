package Juego;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestEstadoMatriz {

	@Test
	public void puedeMoverEspacioVacio() {

		Tablero tablero = new Tablero();
		// 2 4 8 16 |
		// 4 8 16 2 |
		// 8 16 2 4 |
		// 16 2 4 0 |

		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(0, 1, 4);
		tablero.setValorCelda(0, 2, 8);
		tablero.setValorCelda(0, 3, 16);

		tablero.setValorCelda(1, 0, 4);
		tablero.setValorCelda(1, 1, 8);
		tablero.setValorCelda(1, 2, 16);
		tablero.setValorCelda(1, 3, 2);

		tablero.setValorCelda(2, 0, 8);
		tablero.setValorCelda(2, 1, 16);
		tablero.setValorCelda(2, 2, 2);
		tablero.setValorCelda(2, 3, 4);

		tablero.setValorCelda(3, 0, 16);
		tablero.setValorCelda(3, 1, 2);
		tablero.setValorCelda(3, 2, 4);

		assertTrue(tablero.puedeMover());
	}

	@Test
	public void puedeMoverHayUnaSuma() {
		Tablero tablero = new Tablero();
		// 2 4 8 16 |
		// 4 8 2 32 |
		// 8 16 2 4 |
		// 16 2 4 8 |

		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(0, 1, 4);
		tablero.setValorCelda(0, 2, 8);
		tablero.setValorCelda(0, 3, 16);

		tablero.setValorCelda(1, 0, 4);
		tablero.setValorCelda(1, 1, 8);
		tablero.setValorCelda(1, 2, 2);
		tablero.setValorCelda(1, 3, 32);

		tablero.setValorCelda(2, 0, 8);
		tablero.setValorCelda(2, 1, 16);
		tablero.setValorCelda(2, 2, 2);
		tablero.setValorCelda(2, 3, 4);

		tablero.setValorCelda(3, 0, 16);
		tablero.setValorCelda(3, 1, 2);
		tablero.setValorCelda(3, 2, 4);
		tablero.setValorCelda(3, 3, 8);

		assertTrue(tablero.puedeMover());
	}

	@Test
	public void noPuedeMover() {
		Tablero tablero = new Tablero();
		// 2 4 8 16 |
		// 4 8 16 2 |
		// 8 16 2 4 |
		// 16 2 4 8 |

		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(0, 1, 4);
		tablero.setValorCelda(0, 2, 8);
		tablero.setValorCelda(0, 3, 16);

		tablero.setValorCelda(1, 0, 4);
		tablero.setValorCelda(1, 1, 8);
		tablero.setValorCelda(1, 2, 16);
		tablero.setValorCelda(1, 3, 2);

		tablero.setValorCelda(2, 0, 8);
		tablero.setValorCelda(2, 1, 16);
		tablero.setValorCelda(2, 2, 2);
		tablero.setValorCelda(2, 3, 4);

		tablero.setValorCelda(3, 0, 16);
		tablero.setValorCelda(3, 1, 2);
		tablero.setValorCelda(3, 2, 4);
		tablero.setValorCelda(3, 3, 8);

		assertFalse(tablero.puedeMover());
	}

}
