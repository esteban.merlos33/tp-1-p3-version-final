package Juego;

public class Tablero {

	// Tamano del Tablero
	private final int size = 4;
	private Celda[][] tablero = new Celda[size][size];

	// Inicia el Tablero 4x4 con todos los valores en 0
	public Tablero() {

		for (int i = 0; i < tablero[0].length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				tablero[i][j] = new Celda();
			}

		}
	} 

	// Retorna verdadero si hay una casilla disponible
	public boolean casillaVacia() {

		for (int i = 0; i < tablero[0].length; i++) {
			for (int j = 0; j < tablero.length; j++) {

				if (!tablero[i][j].getOcupada() || tablero[i][j].getValor() == 0)
					return true;
			}

		}
		return false;
	}

	// Usado en el dise�o de la parte logica
	public void mostrarTablero() {

		for (int i = 0; i < tablero[0].length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				System.out.print(tablero[i][j].getValor() + " | ");
			}
			System.out.println("");
		}
		System.out.println("\n");
	}

	// Devuelve true si el usuario no puede hacer mas movimientos, de lo contrario
	// devuelve false.
	public boolean gameOver() {
		return (!puedeMover()) ? true : false;
	}

	// Devuelve true si hay un 2048 en el tablero, de lo contrario devuelve false.
	public boolean hayUn2048() {
		// Compara las celdas con el valor "2048" para saber si el jugador alcanz� el
		// valor maximo
		for (int i = 0; i < tablero[0].length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				if (tablero[i][j].getValor() == 2048)
					return true;
			}
		}
		return false;
	}

	public boolean puedeMover() {// Si despues de mover hacia alguna direcci�n se produce un cambio entonces,
									// puede mover.
		Tablero aux1 = new Tablero();
		Tablero aux2 = new Tablero();
		Tablero aux3 = new Tablero();
		Tablero aux4 = new Tablero();
		aux1.copiarTablero(this);
		aux2.copiarTablero(this);
		aux3.copiarTablero(this);
		aux4.copiarTablero(this);

		aux1.moverHaciaAbajo();
		aux2.moverHaciaArriba();
		aux3.moverHaciaIzquierda();
		aux4.moverHaciaDerecha();

		if (this.equals(aux1) && this.equals(aux2) && this.equals(aux3) && this.equals(aux4)) {
			return false;
		}

		return true;
	}

	// Metodo nuevo
	protected void copiarTablero(Tablero otro) {
		for (int i = 0; i < this.getSize(); i++) {
			for (int j = 0; j < this.getSize(); j++) {
				this.setValorCelda(i, j, otro.getTablero()[i][j].getValor());
			}
		}

	}

	public Celda[][] getTablero() {
		return this.tablero;
	}

//--------------------------VACIAR TABLERO---------------------------------
	// Setea todas las celdas del tablero en 0
	public void vaciarTablero() {

		for (int i = 0; i < tablero[0].length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				tablero[i][j].setValor(0);
			}
		}

	}

//--------------------------PUNTAJE TABLERO---------------------------------
	// Devuelve el puntaje actual
	public int getPuntajeActual() {
		int puntaje = 0;
		for (int i = 0; i < tablero[0].length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				puntaje += tablero[i][j].getValor();
			}
		}

		return puntaje;
	}

//--------------------------MOVER HACIA LA DERECHA--------------------------

	public void moverHaciaDerecha() {

		for (int i = 0; i < this.tablero[0].length; i++) {
			sumarHaciaDerecha(this.tablero[i]);
			// int j = buscarCeldaVaciaDer(this.Tablero[i])-1; //buscamos el primer indice
			// vacio menos 1 para ya empezar a buscar
			// OrdenarHaciaLaDerecha(j,this.Tablero[i]); // ordenamos hacia la derecha
			ordenarHaciaDerecha(this.tablero[i]);

		}

	}

	// Dezplaza hacia la derecha las celdas ocupadas
	private void ordenarHaciaDerecha(Celda[] celdas) {
		int j = celdas.length - 1; // arracamos desde celda-1

		for (int i = celdas.length - 1; i >= 0; i--) {
			if (celdas[i].getOcupada()) { // si la celda esta ocupada
				celdas[j].setValor(celdas[i].getValor()); // seteamos la celda j con el valor de la celda i
				if (i != j)
					celdas[i].setValor(0); // si i!=j vaceamos la casilla
				j--;
			} // restamos j
		}

	}

	// Suma los numeros iguales hacia la derecha
	private void sumarHaciaDerecha(Celda[] arr) { // suma los numeros hacia la derecha

		sumarHaciaDerecha(arr.length - 2, arr.length - 1, arr);

	}

	// Metodo recursivo
	private void sumarHaciaDerecha(int indice, int pivote, Celda[] arr) {

		if (pivote <= 0 || indice < 0) // si el pivote es menor a 0, el array fue totalmente recorrido
			return;
		if (arr[pivote].getOcupada()) { // si la celda con el indice de pivote esta ocupada nos fijamos si tienen el
										// mismo valor que el indice

			if (arr[indice].getValor() == arr[pivote].getValor()) { // si tienen el mismo valor sumamos ambas en la
																	// celda con index pivote
				arr[pivote].setValor(arr[pivote].getValor() + arr[indice].getValor());
				arr[indice].setValor(0); // ponemos en 0 la celda con index indice
				sumarHaciaDerecha(indice-1, indice, arr);
			} // seguimos recorriendo. Al valor de indice le damos pivote -2 para recorrer
				// desde una celda != a la de pivote
				// a el argumento pivote le pasamos pivote -1 para solo movernos en la siguiente
				// casilla y volver a preguntar
			else if (arr[indice].getOcupada())
				sumarHaciaDerecha(indice - 1, indice, arr); // si la celda de indice no es igual a la celda de pivote
															// pero es
			// >0 entonces tomamos como nuevo pivote la posicion del indice
			else
				sumarHaciaDerecha(indice - 1, pivote, arr);
		} // seguimos recorriendo con el indice ya que la celda esta vacia
		else
			sumarHaciaDerecha(pivote - 2, pivote - 1, arr); // si el pivote no esta ocupado nos seguimos moviendo hacia
		// atras, el index se situa una posicion menos que el pivote
		// para no repetir valores
	}

//--------------------------MOVER HACIA LA IZQUIERDA--------------------------//

	public void moverHaciaIzquierda() {
		for (int i = 0; i < this.tablero[0].length; i++) {
			sumarHaciaIzquierda(tablero[i]);
			// int j = buscarCeldaVaciaIzq(Tablero[i])+1;
			// ordenarIzq(j,Tablero[i]);
			ordenarHaciaIzquierda(this.tablero[i]);
		}

	}

	// Dezplaza hacia la izquierda las celdas ocupadas
	private void ordenarHaciaIzquierda(Celda[] celdas) { // ordena hacia la izq
		int j = 0; // celdas arracando desde la izq

		for (int i = 0; i < celdas.length; i++) {
			if (celdas[i].getOcupada()) { // si la celda esta ocupada
				celdas[j].setValor(celdas[i].getValor()); // la cambia a la pos j
				if (i != j)
					celdas[i].setValor(0); // si i!=j cambiamos el valor de i a 0 ya que si no rellenamos y luego
											// vaceamos la celda
				j++;
			} // aumentamos la j ya que ahora esta ocupada
		}

	}

	// Suma los numeros iguales hacia la izquierda
	private void sumarHaciaIzquierda(Celda[] celdas) {
		sumarHaciaIzquierda(1, 0, celdas);

	}

	// Metodo recursivo
	private void sumarHaciaIzquierda(int indice, int pivote, Celda[] celdas) {
		if (pivote >= celdas.length || indice >= celdas.length) // si indice o pivote son mayores que el size de celdas
																// terminamos de ordenar
			return;
		if (celdas[pivote].getOcupada()) { // si la celda pivote esta ocupada

			if (celdas[pivote].getValor() == celdas[indice].getValor()) { // nos preguntamos si tiene el mismo valor que
																			// la celda con el index indice

				celdas[pivote].setValor(celdas[pivote].getValor() + celdas[indice].getValor()); // si se esto se cumple,
																								// seteamos la celda
																								// pivote con la suma de
																								// ambos los elem de
																								// ambos index
				celdas[indice].setValor(0); // seteamos el valor de indice a 0
				sumarHaciaIzquierda(indice+1, indice, celdas); // pivote toma el indice de la siguiente celda e
																		// indice
				// se adelanta dos para no comparar con el elem de
				// pivote
			}

			else if (celdas[indice].getOcupada()) // si la celda esta ocupada pero pivote e indice no comparten valor
													// tomamos indice como nuevo pivote
				sumarHaciaIzquierda(indice + 1, indice, celdas);
			else
				sumarHaciaIzquierda(indice + 1, pivote, celdas); // ya que la celda esta vacia seguimos iterando
		} else
			sumarHaciaIzquierda(pivote + 2, pivote + 1, celdas); // si la celda pivote esta vacia iteramos sobre pivote
																	// una
		// vez y dos veces con indice para no comparar con el elem
		// de pivote
	}

//--------------------------MOVER HACIA ARRIBA--------------------------//	

	public void moverHaciaArriba() {
		for (int i = 0; i < this.tablero.length; i++) {
			sumarHaciaArriba(i, this.tablero);
			ordenarHaciaArriba(i, this.tablero);
		}
	}

	// Dezplaza hacia arriba las celdas ocupadas
	private static void ordenarHaciaArriba(int i, Celda[][] matriz) { //
		int index = 0; // 0 //[2,2,4,0] //index=2
						// [0,4,8,16]
		for (int j = 0; j < matriz.length; j++) { // [*j0,8,2,32]
			if (matriz[j][i].getOcupada()) { // [0,8,2,32]

				matriz[index][i].setValor(matriz[j][i].getValor());
				if (index != j)
					matriz[j][i].setValor(0);
				index++;
			}

		}

	}

	// Suma los numeros iguales hacia arriba
	private void sumarHaciaArriba(int i, Celda[][] matriz) {
		sumarHaciaArriba(i, 0, 1, matriz);
	}

	// Suma los numeros iguales hacia arriba
	// Metodo recursivo
	private void sumarHaciaArriba(int posABuscar, int pivote, int indice, Celda[][] matriz) {

		if (indice > matriz.length - 1 || pivote > matriz.length - 1)
			return;

		if (matriz[pivote][posABuscar].getOcupada()) { // si la celda pivote esta ocupada

			if (matriz[indice][posABuscar].equals(matriz[pivote][posABuscar])) { // preguntamos si la celda pivote es
																					// igual a la celda indice
				matriz[pivote][posABuscar].merge(matriz[indice][posABuscar]);
				; // si es igual
					// sumamos
					// ambas
				matriz[indice][posABuscar].setValor(0); // seteamos a 0 el indice // celdas y
														// depositamos
				sumarHaciaArriba(posABuscar, indice, indice + 1, matriz); // el valor
				// en el
			} // index
				// pivote

			else if (matriz[indice][posABuscar].getOcupada()) // si la celda esta ocupada pero es distinta del pivote
				sumarHaciaArriba(posABuscar, indice, indice + 1, matriz); // la celda indice sera el nuevo pivote
			else
				sumarHaciaArriba(posABuscar, pivote, indice + 1, matriz); // si la celda indice esta vacia seguimos
																			// iterando con
			// indice
		} else {
			sumarHaciaArriba(posABuscar, pivote + 1, pivote + 2, matriz); // si pivote esta vacio iteramos con pivote
		}

	}
//----------------------------------------MOVER HACIA ABAJO--------------------------------------------------//

	public void moverHaciaAbajo() {
		for (int i = this.tablero.length - 1; i > -1; i--) {
			sumarHaciaAbajo(i, this.tablero);
			ordenarHaciaAbajo(i, this.tablero);

		}

	}

	// Dezplaza hacia abajo las celdas ocupadas
	private void ordenarHaciaAbajo(int posABuscar, Celda[][] tablero) {
		int bufferCelda = tablero.length - 1;
		for (int i = tablero.length - 1; i > -1; i--) {
			if (tablero[i][posABuscar].getOcupada()) {

				tablero[bufferCelda][posABuscar].setValor(tablero[i][posABuscar].getValor());

				if (bufferCelda != i)
					tablero[i][posABuscar].setValor(0);

				bufferCelda--;
			}

		}

	}

	// Suma los numeros iguales hacia abajo
	private void sumarHaciaAbajo(int posABuscar, Celda[][] tablero) {
		sumarHaciaAbajo(posABuscar, tablero.length - 1, tablero.length - 2, tablero);
	}

	private void sumarHaciaAbajo(int posABuscar, int pivote, int indice, Celda[][] tablero) {

		if (pivote < 0 || indice < 0)
			return;
		if (tablero[pivote][posABuscar].getOcupada()) {

			if (tablero[pivote][posABuscar].equals(tablero[indice][posABuscar])) {
				tablero[pivote][posABuscar].merge(tablero[indice][posABuscar]);
				tablero[indice][posABuscar].setValor(0);
				sumarHaciaAbajo(posABuscar, indice , indice - 1, tablero);
			}

			else if (tablero[indice][posABuscar].getOcupada())
				sumarHaciaAbajo(posABuscar, indice, indice - 1, tablero);

			else
				sumarHaciaAbajo(posABuscar, pivote, indice - 1, tablero);
		} else
			sumarHaciaAbajo(posABuscar, pivote - 1, pivote - 2, tablero);

	}
//------------------------------------------------------------------------------------

// Metodos para test JUnit
	protected void setValorCelda(int fila, int columna, int valor) {
		this.tablero[fila][columna].setValor(valor);
	}

	protected int getValorEnCelda(int fila, int columna) {
		return this.tablero[fila][columna].getValor();
	}

	protected int getSize() {
		return this.size;
	}

	protected Celda getCelda(int fila, int columna) {

		return this.tablero[fila][columna];
	}

	protected boolean equals(Tablero otro) {
		if ((this == null && otro != null) || (this != null && otro == null) || this.getSize() != otro.getSize()) {
			return false;
		}
		boolean sonIguales = true;

		for (int i = 0; i < this.getSize(); i++) {
			for (int j = 0; j < this.getSize(); j++) {
				if (this.getTablero()[i][j].getValor() != otro.getTablero()[i][j].getValor()) {
					sonIguales = false;
				}
			}

		}

		return sonIguales;

	}

}
